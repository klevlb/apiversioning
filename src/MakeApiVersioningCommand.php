<?php

namespace Klevlb\ApiVersioning;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\File;

class MakeApiVersioningCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'api:version {--new}';

    protected $controllerPath = 'app/Http/Controllers/Api';

    protected $servicePath = 'app/Services/Api';

    protected $version;

    protected $previousVersion;
    protected $newControllerPath;

    protected $versionFile;
    protected $newServicePath;
    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create a new api version';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        if (File::isDirectory('version')) {
            $this->versionFile = 'version/api-version.json';
            $this->version = json_decode(file_get_contents($this->versionFile))->version;
        } else {
            $this->versionFile = __DIR__ . '/api-version.json';
            $this->version = json_decode(file_get_contents($this->versionFile))->version;
        }
        $this->previousVersion = $this->version - 1;
        $this->newControllerPath = $this->controllerPath . '/V' . $this->version;
        $this->newServicePath = $this->servicePath . '/V' . $this->version;
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        if ($this->option('new')) {

            //Create the new directory
            $this->createFirstVersion();

            //set the next version that needs to be created
            $this->iterateVersion();

            $this->info("All the old files were moved to a new api folder version");

            return 0;
        }

        $this->createNextVersion();

        $this->iterateVersion();

        $this->info("A new api version folders was created");

        return 0;
    }

    protected function createFirstVersion()
    {
        if (File::isDirectory('app/Services'))
        {
            $this->createServiceDirectory(true);

            $this->moveOldServicesToNewDirectory(true);
        }

        $this->createControllerDirectory(true);


        $this->moveOldControllersToNewDirectory(true);
    }

    protected function createNextVersion()
    {
        if (File::isDirectory('app/Services'))
        {
            $this->createServiceDirectory();

            $this->moveOldServicesToNewDirectory();
        }

        $this->createControllerDirectory();

        $this->moveOldControllersToNewDirectory();
    }


    protected function iterateVersion()
    {
        $newVersion = $this->version + 1;
        file_put_contents($this->versionFile, json_encode(['version' => $newVersion]));
    }

    protected function createControllerDirectory($firstVersion = false)
    {
        if ($firstVersion) {
            $this->createDirectory($this->controllerPath);
        }

        $this->createDirectory($this->controllerPath . '/V' . $this->version);
    }

    protected function createServiceDirectory($firstVersion = false)
    {
        if ($firstVersion) {
            $this->createDirectory($this->servicePath);
        }

        $this->createDirectory($this->servicePath . '/V' . $this->version);
    }

    protected function moveOldServicesToNewDirectory($firstIteration = false)
    {
        if ($firstIteration)
        {
            $services = scandir('app/Services');

            foreach ($services as $service) {
                if ($service === "." || $service === ".." || $service === "Api") {
                    continue;
                }

                if (!str_contains($service, "Service")) {
                    continue;
                }

                $newPathToChange = $this->newServicePath . '/' . $service;
                rename('app/Services/' . $service, $newPathToChange);

                $this->changeNamespace($this->newServicePath . '/', $service, true, true);
            }

            return;
        }

        $services = scandir($this->servicePath . '/V' . $this->previousVersion);

        foreach ($services as $serviceName) {
            if ($serviceName === "." || $serviceName === ".."  || $serviceName === "Api") {
                continue;
            }

            if (!str_contains($serviceName, "Service")) {
                continue;
            }

            $newPathToChange = $this->newServicePath . '/' . $serviceName;
            copy($this->servicePath . '/V' . $this->previousVersion . '/' . $serviceName, $newPathToChange);

            $this->changeNamespace($this->newServicePath . '/', $serviceName, forService: true);
        }
    }

    protected function moveOldControllersToNewDirectory($firstIteration = false)
    {
        if ($firstIteration)
        {
            $controllers = scandir('app/Http/Controllers');

            foreach ($controllers as $controllerName) {
                if ($controllerName === "." || $controllerName === ".." ||
                    $controllerName === "Controller.php" || $controllerName === "Api") {
                    continue;
                }

                if (!str_contains($controllerName, "Controller")) {
                    continue;
                }

                $newPathToChange = $this->newControllerPath . '/' . $controllerName;
                rename('app/Http/Controllers/' . $controllerName, $newPathToChange);

                $this->changeNamespace($this->newControllerPath . '/', $controllerName, true);
            }

            return;
        }

        $controllers = scandir($this->controllerPath . '/V' . $this->previousVersion);

        foreach ($controllers as $controllerName) {
            if ($controllerName === "." || $controllerName === ".." ||
                $controllerName === "Controller.php" || $controllerName === "Api") {
                continue;
            }

            if (!str_contains($controllerName, "Controller")) {
                continue;
            }

            $newPathToChange = $this->newControllerPath . '/' . $controllerName;
            copy($this->controllerPath . '/V' . $this->previousVersion . '/' . $controllerName, $newPathToChange);

            $this->changeNamespace($this->newControllerPath . '/', $controllerName);
        }
    }

    protected function changeNamespace($path, $fileName, $firstIteration = false, $forService = false)
    {
        if (!$forService)
        {
            // Search for the string you want to replace
            $searchString = [
                $firstIteration ? "namespace App\Http\Controllers;" :
                "namespace App\Http\Controllers\Api\V{$this->previousVersion};",
                $firstIteration ? "App\Services" : "App\Services\Api\V{$this->previousVersion}"
                ];

            $replaceString = [
                "namespace App\Http\Controllers\Api\V" . $this->version . ';',
                "App\Services\Api\V" . $this->version
            ];
        } else {
            $searchString = $firstIteration ? "namespace App\Services;" :
                "namespace App\Services\Api\V{$this->previousVersion};";

            $replaceString = "namespace App\Services\Api\V" . $this->version . ';';
        }

        $this->changeFileContent($path, $fileName, $searchString, $replaceString);
    }

    protected function changeFileContent(string $path, string $fileName, mixed $searchString, mixed $replaceString)
    {
        $filename = $path . $fileName;

        $file = fopen($filename, "r");

        // Read the contents of the file
        $contents = fread($file, filesize($filename));


        if (is_array($searchString) && is_array($replaceString)) {
            foreach ($searchString as $key => $search) {
                $contents = str_replace($search, $replaceString[$key], $contents);
            }
        } else {
            $contents = str_replace($searchString, $replaceString, $contents);
        }

        // Close the file
        fclose($file);

        // Open the file for writing
        $file = fopen($filename, "w");

        // Write the modified contents to the file
        fwrite($file, $contents);

        // Close the file
        fclose($file);
    }

    protected function createDirectory($path)
    {
        if (!File::isDirectory($path)) {
            File::makeDirectory($path);
        }
    }
}
