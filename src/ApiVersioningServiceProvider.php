<?php

namespace Klevlb\ApiVersioning;

use Illuminate\Support\ServiceProvider;

class ApiVersioningServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        $this->publishes([
            __DIR__ . '/api-version.json' => base_path('versions/api-version.json'),
        ]);

        $this->commands([
            MakeApiVersioningCommand::class
        ]);
    }
}
